<?php

/**
 * Implements hook_drush_command().
 *
 * @return
 *   An associative array describing your command(s).
 *
 * @see drush_parse_command()
 */
function rules_drush_drush_command() {
  $items = array();

  $items['rules-invoke-component'] = array(
    'description' => "List all the active and inactive rules for your site.",
    'drupal dependencies' => array('rules'),
    'arguments' => array(
      'rule' => 'Component name to invoke.',
    ),
    'aliases' => array('ric'),
  );

  return $items;
}

function drush_rules_drush_rules_invoke_component($component_name){
  rules_invoke_component($component_name);
}